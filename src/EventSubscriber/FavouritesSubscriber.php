<?php

namespace Drupal\pagedesigner_favourite\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\pagedesigner\ElementEvents;
use Drupal\pagedesigner\Event\ElementEvent;
use Drupal\pagedesigner_favourite\Plugin\rest\resource\FavouriteRessource;
use Drupal\user\UserData;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to update pattern for favourites.
 */
class FavouritesSubscriber implements EventSubscriberInterface {

  /**
   * Create the UpdateCategorySubscriber.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\user\UserData $userData
   *   The user data service.
   */
  public function __construct(
    protected AccountProxyInterface $currentUser,
    protected UserData $userData
  ) {
  }

  /**
   * Add the necessary library.
   *
   * @param \Drupal\pagedesigner\Event\ElementEvent $event
   *   The event.
   */
  public function addAttachment(ElementEvent $event) {
    if ($this->currentUser->hasPermission('restful post pagedesigner_favourite')) {
      $attachments = &$event->getData()[0];
      $attachments['library'][] = 'pagedesigner_favourite/pagedesigner';
    }
  }

  /**
   * Update the data for favourite patterns.
   *
   * @param \Drupal\pagedesigner\Event\ElementEvent $event
   *   The event.
   */
  public function setFavourites(ElementEvent $event) {
    $key = FavouriteRessource::KEY;
    $user_id = $this->currentUser->id();
    $user_data = $this->userData->get($key, $user_id, $key);
    $favourites = Json::decode((string) $user_data);
    if (!$favourites) {
      $favourites = [];
    }

    $patterns = &$event->getData()[0];
    foreach ($patterns as $id => $pattern) {
      if (!empty($favourites[$id])) {
        $patterns[$id]['additional']['favourite'] = 1;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ElementEvents::COLLECTATTACHMENTS_AFTER => [['addAttachment', 50]],
      ElementEvents::ADAPTPATTERNS_AFTER => [['setFavourites', 50]],
    ];
  }

}
