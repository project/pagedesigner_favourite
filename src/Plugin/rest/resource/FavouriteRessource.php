<?php

namespace Drupal\pagedesigner_favourite\Plugin\rest\resource;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\user\UserData;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "pagedesigner_favourite",
 *   label = @Translation("Pagedesigner favourite"),
 *   uri_paths = {
 *     "canonical" = "/pagedesigner/favourite/{id}",
 *     "create" = "/pagedesigner/favourite",
 *   }
 * )
 */
class FavouriteRessource extends ResourceBase {


  /**
   * The storage key.
   *
   * @var string
   */
  final public const KEY = 'pagedesigner_favourite';

  /**
   * Constructs a new ElementResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   A current user instance.
   * @param \Drupal\user\UserData $userData
   *   The user data service.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        protected AccountProxyInterface $currentUser,
        protected UserData $userData) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('pagedesigner'),
          $container->get('current_user'),
          $container->get('user.data')
      );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   If the request is malformed.
   */
  public function get($id = NULL) {
    $patterns = Json::decode((string) $this->userData->get(static::KEY, $this->currentUser->id(), static::KEY));
    if (!empty($id)) {
      if (!empty($patterns) && !empty($patterns[$id])) {
        $response = new ResourceResponse([TRUE], 200);
      }
      else {
        $response = new ResourceResponse([FALSE], 200);
      }
    }
    else {
      $response = new ResourceResponse([$patterns], 200);
    }
    $response->addCacheableDependency(['cache' => ['max-age' => 0]]);
    return $response;
  }

  /**
   * Responds to POST requests.
   *
   *   The request.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   If the request is malformed.
   */
  public function post(Request $request) {
    $requestContent = Json::decode((string) $request->getContent());
    if (empty($requestContent['id'])) {
      throw new BadRequestHttpException('The id is mandatory for the post requests.');
    }
    $pattern = $requestContent['id'];
    $patterns = Json::decode((string) $this->userData->get(static::KEY, $this->currentUser->id(), static::KEY));
    $patterns[$pattern] = $pattern;
    $this->userData->set(static::KEY, $this->currentUser->id(), static::KEY, json_encode($patterns, JSON_THROW_ON_ERROR));
    $response = new ModifiedResourceResponse([$pattern], 201);
    return $response;
  }

  /**
   * Responds to DELETE requests.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   If the request is malformed.
   */
  public function delete($id = NULL) {
    if (empty($id)) {
      throw new BadRequestHttpException('The id is mandatory for the delete requests.');
    }
    $response = new ResourceResponse(NULL, 404);
    $patterns = Json::decode((string) $this->userData->get(static::KEY, $this->currentUser->id(), static::KEY));
    if (!empty($patterns[$id])) {
      unset($patterns[$id]);
      $this->userData->set(static::KEY, $this->currentUser->id(), static::KEY, json_encode($patterns, JSON_THROW_ON_ERROR));
      $response = new ModifiedResourceResponse(NULL, 204);
    }
    return $response;
  }

}
